var express = require('express');
var router = express.Router();
var DB = [];
/* route http from browser */
router.get('/', function(req, res, next) {
  res.render('demo/index', { title: 'Tutorial NodeJS' });
});
/*action update*/
router.post('/', function(req, res, next) {
  var data = req.body;
  data._id = 10000 + int(10000,1000000);
  if (data !== undefined) {
    DB.push(data);
  }
  console.log(JSON.stringify(DB));
  res.send(DB);
});

/*action delete*/
router.delete('/', function(req, res, next) {
  var data = req.body;
  for (var i = 0; i < DB.length; i++) {
    if(parseInt(DB[i]._id) === parseInt(data.id)){
      delete DB[i];
    }
  }
  res.send(DB);
});
/*action update*/
router.put('/', function(req, res, next) {
  console.log("UPDATE: "+JSON.stringify(req.body));
  res.send(JSON.stringify(req.body));
});

function int(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
module.exports = router;
