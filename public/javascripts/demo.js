$(document).ready(function() {


    petition(null, 'POST', generarTabla);
    // (1)
    var evento = function() {
        // (2)
        petition({
            'nombre': $("input[name='nombre']").val(),
            'correo': $("input[name='correo']").val(),
            'telefono': $("input[name='telefono']").val()
        }, 'POST', generarTabla);
    };
    // (5)

    $("#boton").click(evento); // (1)

    $("identificador").click(eventDelete);
});

function generarTabla(salida) {
    //clear table
    $("table>tbody").find("tr").remove();
    //show array
    for (var i = 0; i < salida.length; i++) {
        if (salida[i] !== null && salida[i].nombre !== undefined) {
            var boton  = '<button type="button" onClick="eventDelete('+salida[i]._id+')" class="btn btn-danger">Danger</button>';
            var filas = "<td>" + salida[i]._id + "</td>" +
                "<td>" + salida[i].nombre + "</td>" +
                "<td>" + salida[i].correo + "</td>" +
                "<td>" + salida[i].telefono + "</td>"+
                "<td>" + boton + "</td>";
            var html = "<tr>" + filas +"</tr>";
            // write html
            $("table>tbody").append(html);
        }
    }
}
// (2)
function petition(info, metod, callback) {
    // (3)
    $.ajax({
        url: "/",
        type: metod,
        data: info
    }).done(function(res) {
        // (4)
        callback(res);
    });
}

function eventDelete(id){
      petition({"id" : id}, 'DELETE', generarTabla);
}
